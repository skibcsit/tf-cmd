import domain.User
import lib.{ProgramCmd, ProgramHelp}

object Main {
  def main(args: Array[String]): Unit = {

    println(ProgramHelp.program + "\n")

    val testArgs = args // testArgs = Array("--name=Иван", "--surname=Иванов", "--age=12")

    val result = ProgramCmd.program(testArgs)

    val user: Option[User] = ProgramCmd.transform(
      result,
      { case List(name, surname, age) => User(name, surname, age)},
    )

    println(user)
  }
}
