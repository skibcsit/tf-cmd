package domain

case class User(
                 name: String,
                 surname: String,
                 age: String
               )
