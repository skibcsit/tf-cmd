trait ProgramAlgebra {
  type TProgram
  type TFunction
  def emptyProgram(name: String): TProgram
  def addFunction(program: TProgram, function: TFunction): TProgram
  def function(name: String, argNames: List[String]): TFunction
}

trait HelpInterpreter extends ProgramAlgebra {
  type TProgram = String
  type TFunction = String
  def emptyProgram(name: String): String = s"Program $name usage:"
  def addFunction(program: String, function: String): String =
    program + s"\n  <propgram> $function"
  def function(name: String, argNames: List[String]): String =
    s"$name " + argNames.map(argName => s"--$argName <value>").mkString(" ")
}

trait MyProgram extends ProgramAlgebra {
  def program =
    addFunction(
      addFunction(
        emptyProgram("myprog"),
        function("one", List("x", "y")),
      ),
      function("two", List("param")),
    )
}

object MyProgramHelp extends MyProgram with HelpInterpreter

MyProgramHelp.program
