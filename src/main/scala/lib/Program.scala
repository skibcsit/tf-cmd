package lib

trait Program extends ProgramAlgebra {

  val argNames: List[String] = List("name", "surname", "age")

  def impl: TFunctionImpl

  def program: TProgram =
    addFunction(
      emptyProgram("program"),
      functionDecl("declaration", argNames),
      impl
    )

}
