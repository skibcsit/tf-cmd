package lib

trait ProgramAlgebra {
  type TProgram
  type TFunctionDecl
  type TFunctionImpl

  def emptyProgram(name: String): TProgram

  def addFunction(
                   program: TProgram,
                   decl: TFunctionDecl,
                   impl: TFunctionImpl
                 ): TProgram

  def functionDecl(name: String, argNames: List[String]): TFunctionDecl
}
